// Copyright John W. Wilkinson 2007 - 2014
// Distributed under the MIT License, see accompanying file LICENSE.txt

#ifndef JSON_SPIRIT
#define JSON_SPIRIT

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

#ifdef JSON_SPIRIT_USE_CONFIG
#include "json_spirit_config.h"
#endif

#include "json_spirit_reader.h"
#include "json_spirit_utils.h"
#include "json_spirit_value.h"
#include "json_spirit_writer.h"

#endif
