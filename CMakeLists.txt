# specify minimum cmake version
cmake_minimum_required(VERSION 3.10)

# specify project properties
project(
  json_spirit
  VERSION 4.08
  DESCRIPTION "A C++ JSON Parser/Generator Implemented with Boost Spirit")

# specify the C++ standard
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED True)

# specify project options
option(
  JSON_SPIRIT_USE_CONFIG
  "Use a configuration file (json_spirit_config.h.in) for setting compile time settings"
  ON)

option(JSON_SPIRIT_VALUE_ENABLED "Enable Value class implementation" ON)
option(JSON_SPIRIT_WVALUE_ENABLED "Enable Value class with Unicode support" ON)
option(JSON_SPIRIT_MVALUE_ENABLED "Enable std::map implementation" ON)
option(JSON_SPIRIT_WMVALUE_ENABLED
       "Enable std::map implementation with Unicode support" ON)

option(JSON_SPIRIT_THREADSAFE "Enable multithread use" OFF)

option(JSON_SPIRIT_BUILD_DEMOS "Compile demos" OFF)
option(JSON_SPIRIT_BUILD_TESTS "Compile tests" OFF)

if(JSON_SPIRIT_USE_CONFIG)
  # use the configuration file
  add_compile_definitions(JSON_SPIRIT_USE_CONFIG)
  configure_file(json_spirit_config.h.in json_spirit_config.h)
  include_directories(${PROJECT_BINARY_DIR})
endif()

find_package(Boost 1.34 REQUIRED)
include_directories(${Boost_INCLUDE_DIR})

add_subdirectory(json_spirit)

if(JSON_SPIRIT_BUILD_DEMOS)
  add_subdirectory(json_demo)
  add_subdirectory(json_headers_only_demo)
  add_subdirectory(json_map_demo)
endif()

if(JSON_SPIRIT_BUILD_TESTS)
  add_subdirectory(json_test)
endif()

install(
  FILES ${PROJECT_SOURCE_DIR}/json_spirit/json_spirit.h
        ${PROJECT_SOURCE_DIR}/json_spirit/json_spirit_error_position.h
        ${PROJECT_SOURCE_DIR}/json_spirit/json_spirit_reader.h
        ${PROJECT_SOURCE_DIR}/json_spirit/json_spirit_reader_template.h
        ${PROJECT_SOURCE_DIR}/json_spirit/json_spirit_stream_reader.h
        ${PROJECT_SOURCE_DIR}/json_spirit/json_spirit_utils.h
        ${PROJECT_SOURCE_DIR}/json_spirit/json_spirit_value.h
        ${PROJECT_SOURCE_DIR}/json_spirit/json_spirit_writer.h
        ${PROJECT_SOURCE_DIR}/json_spirit/json_spirit_writer_template.h
        ${PROJECT_SOURCE_DIR}/json_spirit/json_spirit_writer_options.h
  DESTINATION include)

install(FILES ${PROJECT_BINARY_DIR}/json_spirit/libjson_spirit.a
        DESTINATION lib)

include(CPack)
